from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, RadioField, SelectField, TextAreaField
from wtforms.validators import DataRequired, Length
from wtforms.widgets import TextArea

class aesForm(FlaskForm):
    keysize = RadioField('keysize',choices=[('128','128-bit'),('192','192-bit'),('256','256-bit')],validators=[DataRequired()])
    keytext = StringField('keytext (Hex Char)', validators=[DataRequired()])
    text = TextAreaField('Messages/CipherText (ASCII)', validators=[DataRequired()],widget=TextArea())
    encdec = RadioField('encdec',choices=[('encrypt','Encryption'),('decrypt','Decryption')],validators=[DataRequired()])
    calculate = SubmitField('calculate')

class desForm(FlaskForm):
    keytext = StringField('keytext (16-Byte)', validators=[DataRequired()])
    text = TextAreaField('Messages/CipherText (ASCII)', validators=[DataRequired()],widget=TextArea())
    encdec = RadioField('encdec',choices=[('encrypt','Encryption'),('decrypt','Decryption')],validators=[DataRequired()])
    calculate = SubmitField('calculate')