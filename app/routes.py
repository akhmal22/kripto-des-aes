from flask import render_template,flash,redirect
from app import app
from app.forms import aesForm, desForm
import random
from Crypto.Cipher import DES3
from Crypto.Cipher import AES
from Crypto.Util import Counter
from Crypto import Random

@app.route('/')
@app.route('/index')
def index():
    user = { 'username': 'Akhmal'}
    return render_template('index.html',title='Home',user=user)

@app.route('/aes', methods=['GET','POST'])
def aes():
    form = aesForm()
    if form.validate_on_submit():
        raw_key = form.keytext.data
        raw_msg = form.text.data
        key = b""
        msg = b""
        if form.keysize.data=="128":
            if validateKey(raw_key,32)==True:
                key = bytes.fromhex(raw_key)
            elif len(raw_key)==16:
                key = bytes(raw_key,"ascii")
            else:
                flash("Key must 16-byte String")
                return redirect('/aes')
        elif form.keysize.data=="192":
            if validateKey(raw_key,48)==True:
                key = bytes.fromhex(raw_key)
            elif len(raw_key)==24:
                key = bytes(raw_key,"ascii")
            else:
                flash("Key must 24-byte String")
                return redirect('/aes')
        else:
            if validateKey(raw_key,64)==True:
                key = bytes.fromhex(raw_key)
            elif len(raw_key)==32:
                key = bytes(raw_key,"ascii")
            else:
                flash("Key must 32-byte String")
                return redirect('/aes')
        
        bbs = blumblumshub(933287902892,(13231090003*1853105600379),101,random.randint(1,100)).lstrip("0x").zfill(20)
        bbs = bytes.fromhex(bbs)
        nonce = bbs[0:8]
        ctr = Counter.new(int(AES.block_size*8/2), prefix=nonce)
        cipher = AES.new(key, AES.MODE_CTR,nonce=ctr)
        msg = bytes(raw_msg,"ascii")
        result = b""
        resval = ""
        if form.encdec.data=="encrypt":
            result = cipher.encrypt(msg)
            resval = "ciphertext (hex)"
        else:
            result = cipher.decrypt(msg)
            resval = "plaintext (hex)"
        flash("Key: {}".format(key))
        flash("Text: {}".format(msg))
        flash("Nonce: {}".format(nonce))
        flash("{}: {}".format(resval,result.hex()))

    return render_template('aes.html', title='AES', form=form)

@app.route('/des',methods=['GET','POST'])
def des():
    form = desForm()
    if form.validate_on_submit():
        raw_key = form.keytext.data
        raw_msg = form.text.data
        key = b""
        msg = b""
        if validateKey(raw_key,32)==True:
            key = bytes.fromhex(raw_key)
        elif len(raw_key)==16:
            key = bytes(raw_key,"ascii")
        else:
            flash("Key must 16-byte String")
            return redirect('/des')
        
        bbs = blumblumshub(933287902892,(13231090003*1853105600379),101,random.randint(1,100)).lstrip("0x").zfill(20)
        bbs = bytes.fromhex(bbs)
        # key = b'Sixteen byte key'
        nonce = bbs[0:4]
        ctr = Counter.new(int(DES3.block_size*8/2), prefix=nonce)
        result = b""
        try:
            cipher = DES3.new(key, DES3.MODE_CTR, counter=ctr)
            msg = bytes(raw_msg,"ascii")
            result = b""
            resval = ""
            if form.encdec.data=="encrypt":
                result = nonce + cipher.encrypt(msg)
                resval = "ciphertext (hex)"
            else:
                result = nonce + cipher.decrypt(msg)
                resval = "plaintext (hex)"
            flash("Key: {}".format(key))
            flash("Text: {}".format(msg))
            flash("Nonce: {}".format(nonce))
            flash("{}: {}".format(resval,result.hex()))
        except :
            flash("Try another Key!")
        

    return render_template('des.html', title='DES', form=form)


def hexCheck(val):
    try:
        int(val,16)
        return True
    except ValueError:
        return False


def validateKey(val,must):
    if hexCheck(val)==True and len(val)==must:
        return True
    else:
        return False


def blumblumshub(s,n,iter,idx):
    x = [0]*iter
    b = [None]*iter
    x[0] = int((s**2)%n)
    for i in range(1,iter):
        x[i] = (x[i-1]**2)%n
        b[i] = x[i]%2
    return hex(x[idx])