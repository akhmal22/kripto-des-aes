from base64 import b64encode
from Crypto.Cipher import AES
from Crypto.Cipher import DES3
from Crypto.Random import get_random_bytes
from Crypto.Util import Counter
from Crypto import Random
import codecs
import random
import json


def blumblumshub(s,n,iter,idx):
    x = [0]*iter
    b = [None]*iter
    x[0] = int((s**2)%n)
    for i in range(1,iter):
        x[i] = (x[i-1]**2)%n
        b[i] = x[i]%2
    return hex(x[idx])


def hexCheck(val):
    try:
        int(val,16)
        return True
    except ValueError:
        return False


# def validateHexKey()


def aesCTRenc(data,hex_key,hex_nonce):
    key = bytes.fromhex(hex_key)
    nonce = bytes.fromhex(hex_nonce)
    cipher = AES.new(key, AES.MODE_CTR,nonce=nonce)
    ct_bytes = cipher.encrypt(data)
    return ct_bytes


def des3CFBenc(data,):
    # key = bytes.fromhex(hex_key)
    #nonce = bytes.fromhex(hex_nonce)
    while True:
        try:
            key = DES3.adjust_key_parity(get_random_bytes(24))
            break
        except ValueError:
            pass

    cipher = DES3.new(key,DES3.MODE_CFB)
    return cipher.encrypt(data)


'''
bd = b"secret_fuck_you_too"
hk = "6f0b9f263835cb21d6d474d146b797ce"
hn = "b7d6f54f40824929"

bddes = b"We are no longer the knights who say ni!"
hkdes = "13a6b2221b0047f1823dce764306c8691d4e8586d8ce4a7b"
#print(aesCTRenc(bd,hk,hn))
print(des3CFBenc(bddes))
'''
'''
key = b'Sixteen byte key'
#nonce = b""
#nonce = Random.new().read(int(DES3.block_size/2))
bbs = blumblumshub(933287902892,(13231090003*1853105600379),101,random.randint(1,100)).lstrip("0x").zfill(20)
print(bbs)
print(len(bbs))
bbs = bytes.fromhex(bbs)
nonce = bbs[0:4]

if len(bbs) == int(DES3.block_size/2):
    nonce = bbs
else:
    nonce = bbs[0:2]

ctr = Counter.new(int(DES3.block_size*8/2), prefix=nonce)
cipher = DES3.new(key, DES3.MODE_CTR, counter=ctr)
#plaintext = b'We are no longer the knights who say ni!'
#msg = nonce + cipher.encrypt(plaintext)
#msg = codecs.decode(msg.hex(),"hex").decode('utf-16')
print(key)
print(len(key))
print(nonce)
print(len(nonce))
'''
'''
key = b'Sixteen byte key'
nonce = Random.new().read(int(DES3.block_size/2))
ctr = Counter.new(int(DES3.block_size*8/2), prefix=nonce)
cipher = DES3.new(key, DES3.MODE_CTR, counter=ctr)
plaintext = b'We are no longer the knights who say ni!'
msg = nonce + cipher.encrypt(plaintext)
print(msg)
'''
data = b"secret"
key = get_random_bytes(32)
cipher = AES.new(key, AES.MODE_CTR,nonce=b"del apan")
ct_bytes = cipher.encrypt(data)
nonce = b64encode(cipher.nonce).decode('utf-8')
ct = b64encode(ct_bytes).decode('utf-8')
result = json.dumps({'nonce':nonce, 'ciphertext':ct})
print(result)
print(cipher.nonce)
print(len(cipher.nonce))